#!/bin/bash

PATH=/usr/local/bin:/usr/local/sbin:/opt/local/bin:/usr/bin:/usr/sbin:/bin:/sbin
SHELL=/bin/bash

_TODAY=`date +%y%m%d`
_NOW=`date +%y%m%d-%H%M`
_L_VMFAMILY=XEN
_L_VM_TEST=`uname -a 2>&1`
if [[ "$_L_VM_TEST" =~ beng ]] ; then
  _L_VMFAMILY="VS"
fi

check_id () {
  _USER=$1
  _ID_EXISTS=$(getent passwd $_USER 2>&1)
  if [ -z "$_ID_EXISTS" ] ; then
    _DO_NOTHING=YES
  elif [[ "$_ID_EXISTS" =~ "$_USER" ]] ; then
    echo "ERROR: $_USER username is already taken"
    echo "Please choose different username"
    exit 1
  else
    echo "ERROR: $_USER username check failed"
    echo "Please try different username"
    exit 1
  fi
  if [ "$_USER" = "admin" ] || [ "$_USER" = "hostmaster" ] || [ "$_USER" = "barracuda" ] || [ "$_USER" = "octopus" ] || [ "$_USER" = "boa" ] ; then
    echo "ERROR: $_USER is a restricted username, please choose different _USER"
    exit 1
  elif [[ "$_USER" =~ "aegir" ]] || [[ "$_USER" =~ "drupal" ]] || [[ "$_USER" =~ "drush" ]] || [[ "$_USER" =~ "sites" ]] || [[ "$_USER" =~ "default" ]] ; then
    echo "ERROR: $_USER includes restricted keyword, please choose different _USER"
    exit 1
  fi
  _REGEX="^[[:digit:]]"
  if [[ "$_USER" =~ "$_REGEX" ]] ; then
    echo "ERROR: $_USER is a wrong username, it should start with a letter, not digit"
    exit 1
  fi
}

fix_dns_settings () {
  mkdir -p /var/backups
  rm -f /var/backups/resolv.conf.tmp
  if [ -e "/etc/resolv.conf" ] ; then
    cp -a /etc/resolv.conf /var/backups/resolv.conf.tmp
  fi
  if [ ! -e "/var/backups/resolv.conf.vanilla" ] ; then
    for Pre in `ls -la /var/backups/resolv.conf.pre-*` ; do
      if [ -e "$Pre" ] && [ -f "$Pre" ] && [ ! -L "$Pre" ] ; then
        cp -a $Pre /var/backups/resolv.conf.vanilla
      fi
    done
  fi
  if [ ! -e "/var/backups/resolv.conf.vanilla" ] ; then
    rm -f /etc/resolv.conf
    cp -a /var/backups/resolv.conf.tmp /etc/resolv.conf
    cp -a /var/backups/resolv.conf.tmp /var/backups/resolv.conf.vanilla
  fi
  sed -i "/^$/d" /var/backups/resolv.conf.vanilla &> /dev/null
  rm -f /etc/resolv.conf
  echo "nameserver 8.8.8.8" > /etc/resolv.conf
  echo "nameserver 8.8.4.4" >> /etc/resolv.conf
  _BROKEN_DNS_TEST=$(host -a omega8.cc 8.8.8.8 -w 5 2>&1)
  if [[ "$_BROKEN_DNS_TEST" =~ "no servers could be reached" ]] && [ -e "/var/backups/resolv.conf.vanilla" ] ; then
    touch /root/.use.local.nameservers.cnf
    rm -f /etc/resolv.conf
    cp -a /var/backups/resolv.conf.vanilla /etc/resolv.conf
    echo "nameserver 8.8.8.8" >> /etc/resolv.conf
    echo "nameserver 8.8.4.4" >> /etc/resolv.conf
  else
    rm -f /etc/resolv.conf
    cp -a /var/backups/resolv.conf.tmp /etc/resolv.conf
  fi
  if [ -e "/etc/pdnsd.conf" ] ; then
    if [ -e "/root/.local.dns.IP.list" ] ; then
      sed -i "s/.*127.0.0.1.*//g; s/ *$//g; /^$/d" /root/.local.dns.IP.list
      sed -i "s/.*Dynamic.*//g; s/ *$//g; /^$/d"   /root/.local.dns.IP.list
      _BROKEN_DNS_TEST=$(grep "\." /root/.local.dns.IP.list 2>&1)
      if [ -z "$_BROKEN_DNS_TEST" ] ; then
        echo "        label = \"google-servers\";" > /root/.local.dns.IP.list
        echo "        ip=8.8.8.8;" >> /root/.local.dns.IP.list
        echo "        ip=8.8.4.4;" >> /root/.local.dns.IP.list
      fi
    fi
    _CUSTOM_DNS_TEST=$(grep 8.8.8.8 /etc/pdnsd.conf 2>&1)
    _BROKEN_DNS_CONF=$(grep "ip=Dynamic" /etc/pdnsd.conf 2>&1)
    if [[ "$_CUSTOM_DNS_TEST" =~ "8.8.8.8" ]] || [[ "$_BROKEN_DNS_CONF" =~ "Dynamic" ]] ; then
      if [ -e "/root/.use.local.nameservers.cnf" ] ; then
        echo "        label = \"google-servers\";" > /root/.local.dns.IP.list
        for _IP in `cat /etc/resolv.conf | cut -d ' ' -f2 | sort | uniq`;do echo "        ip=$_IP;" >> /root/.local.dns.IP.list;done
        sed -i "s/ip=8.8.*//g; s/ *$//g; /^$/d" /etc/pdnsd.conf
        sed -i "s/ip=127.*//g; s/ *$//g; /^$/d" /etc/pdnsd.conf
        sed -i "s/ip=Dyn.*//g; s/ *$//g; /^$/d" /etc/pdnsd.conf
        sed -i "s/.*127.0.0.1.*//g; s/ *$//g; /^$/d" /root/.local.dns.IP.list
        sed -i "s/.*Dynamic.*//g; s/ *$//g; /^$/d"   /root/.local.dns.IP.list
        _BROKEN_DNS_TEST=$(grep "\." /root/.local.dns.IP.list 2>&1)
        if [ -z "$_BROKEN_DNS_TEST" ] ; then
           echo "        ip=8.8.8.8;" >> /root/.local.dns.IP.list
           echo "        ip=8.8.4.4;" >> /root/.local.dns.IP.list
        fi
        sed -i '/        label = \"google-servers\";/ {r /root/.local.dns.IP.list
d;};' /etc/pdnsd.conf
        resolvconf -u                      &> /dev/null
        service pdnsd stop                 &> /dev/null
        rm -f /var/cache/pdnsd/pdnsd.cache &> /dev/null
        service pdnsd start                &> /dev/null
        service pdnsd stop                 &> /dev/null
        rm -f /var/cache/pdnsd/pdnsd.cache &> /dev/null
        service pdnsd start                &> /dev/null
      fi
    fi
  fi
}

check_dns_settings () {
  _BROKEN_DNS_TEST=$(host -a omega8.cc 8.8.8.8 -w 5 2>&1)
  if [[ "$_BROKEN_DNS_TEST" =~ "no servers could be reached" ]] || [ -e "/root/.use.local.nameservers.cnf" ] ; then
    _USE_PROVIDER_DNS=YES
    fix_dns_settings
  else
    _USE_PROVIDER_DNS=NO
  fi
}

octopus_install ()
{
  if [ -e "/var/backups/OCTOPUS.sh.txt" ] ; then
    if [ -z "$user" ] ; then
      user="o1"
    fi
    cp -af /var/backups/OCTOPUS.sh.txt /var/backups/OCTOPUS.sh.$user
    _CHECK_HOST=`uname -n`
    if [ "$cmmand" = "in-octopus" ] ; then
      if [ ! -z "$copt" ] ; then
        sed -i "s/^_CLIENT_OPTION=.*/_CLIENT_OPTION=$copt/g" /var/backups/OCTOPUS.sh.$user
      fi
      if [ ! -z "$csub" ] ; then
        sed -i "s/^_CLIENT_SUBSCR=.*/_CLIENT_SUBSCR=$csub/g" /var/backups/OCTOPUS.sh.$user
      fi
      if [ ! -z "$ccor" ] ; then
        sed -i "s/^_CLIENT_CORES=.*/_CLIENT_CORES=$ccor/g"   /var/backups/OCTOPUS.sh.$user
      fi
    else
      sed -i "s/^_MY_EMAIL=.*/_MY_EMAIL=\"$email\"/g"        /var/backups/OCTOPUS.sh.$user
    fi
    sed -i "s/^_CLIENT_EMAIL=.*/_CLIENT_EMAIL=\"$email\"/g"  /var/backups/OCTOPUS.sh.$user
    sed -i "s/^_DNS_SETUP_TEST=.*/_DNS_SETUP_TEST=NO/g"      /var/backups/OCTOPUS.sh.$user
    sed -i "s/^_USER=.*/_USER=$user/g"                       /var/backups/OCTOPUS.sh.$user
    if [ "$mode" = "mini" ] ; then
      sed -i "s/^_AUTOPILOT=.*/_AUTOPILOT=YES/g"             /var/backups/OCTOPUS.sh.$user
    elif [ "$mode" = "max" ] ; then
      sed -i "s/^_AUTOPILOT=.*/_AUTOPILOT=YES/g"             /var/backups/OCTOPUS.sh.$user
      sed -i "s/^_PLATFORMS_LIST=.*/_PLATFORMS_LIST=ALL/g"   /var/backups/OCTOPUS.sh.$user
    elif [ "$mode" = "none" ] ; then
      sed -i "s/^_AUTOPILOT=.*/_AUTOPILOT=YES/g"             /var/backups/OCTOPUS.sh.$user
      sed -i "s/^_PLATFORMS_LIST=.*/_PLATFORMS_LIST=none/g"  /var/backups/OCTOPUS.sh.$user
    else
      sed -i "s/^_PLATFORMS_LIST=.*/_PLATFORMS_LIST=ALL/g"   /var/backups/OCTOPUS.sh.$user
    fi
    if [ -e "/root/.host8.cnf" ] || [[ "$_CHECK_HOST" =~ ".host8." ]] || [ "$_L_VMFAMILY" = "VS" ] ; then
      if [[ "$_CHECK_HOST" =~ ".qq.o8.io" ]] ; then
        _DO_NOTHING=YES
      else
        sed -i "s/^_AUTOPILOT=.*/_AUTOPILOT=YES/g"                   /var/backups/OCTOPUS.sh.$user
        sed -i "s/^_STRONG_PASSWORDS=.*/_STRONG_PASSWORDS=YES/g"     /var/backups/OCTOPUS.sh.$user
      fi
    fi
    if [ "$cmmand" = "in-vanilla" ] ; then
      sed -i "s/^_BRANCH_PROVISION=.*/_BRANCH_PROVISION=6.x-2.x/g"   /var/backups/OCTOPUS.sh.$user
    elif [ "$cmmand" = "in-modern" ] ; then
      sed -i "s/^_BRANCH_PROVISION=.*/_BRANCH_PROVISION=2.3.x-dev/g" /var/backups/OCTOPUS.sh.$user
    else
      sed -i "s/^_BRANCH_PROVISION=.*/_BRANCH_PROVISION=2.3.x-dev/g" /var/backups/OCTOPUS.sh.$user
    fi
    bash /var/backups/OCTOPUS.sh.$user
    sed -i "s/^127.0.1.1.*//g" /etc/hosts
    sed -i "/^$/d" /etc/hosts
  else
    echo "OCTOPUS.sh.txt installer not available - try again"
    exit 1
  fi
}

barracuda_install ()
{
if [ -e "/var/backups/BARRACUDA.sh.txt" ] ; then
  sed -i "s/^_MY_EMAIL=.*/_MY_EMAIL=\"$email\"/g"          /var/backups/BARRACUDA.sh.txt
  sed -i "s/^_AUTOPILOT=.*/_AUTOPILOT=YES/g"               /var/backups/BARRACUDA.sh.txt
  if [ "$kind" = "local" ] ; then
    sed -i "s/^_EASY_LOCALHOST=.*/_EASY_LOCALHOST=YES/g"   /var/backups/BARRACUDA.sh.txt
    sed -i "s/^_EASY_PUBLIC=.*/_EASY_PUBLIC=NO/g"          /var/backups/BARRACUDA.sh.txt
    sed -i "s/^127.0.1.1.*//g" /etc/hosts
    echo "127.0.1.1 aegir.local o1.sub.aegir.local o2.sub.aegir.local o3.sub.aegir.local" >> /etc/hosts
  elif [ "$kind" = "public" ] ; then
    sed -i "s/^_EASY_LOCALHOST=.*/_EASY_LOCALHOST=NO/g"    /var/backups/BARRACUDA.sh.txt
    sed -i "s/^_EASY_PUBLIC=.*/_EASY_PUBLIC=YES/g"         /var/backups/BARRACUDA.sh.txt
    sed -i "s/^_EASY_HOSTNAME=.*/_EASY_HOSTNAME=$fqdn/g"   /var/backups/BARRACUDA.sh.txt
  fi
  if [ ! -z "$rkey" ] ; then
    sed -i "s/^_NEWRELIC_KEY=.*/_NEWRELIC_KEY=\"$rkey\"/g" /var/backups/BARRACUDA.sh.txt
  fi
  if [ "$cmmand" = "in-vanilla" ] ; then
    sed -i "s/^_BRANCH_PROVISION=.*/_BRANCH_PROVISION=6.x-2.x/g"   /var/backups/BARRACUDA.sh.txt
  elif [ "$cmmand" = "in-modern" ] ; then
    sed -i "s/^_BRANCH_PROVISION=.*/_BRANCH_PROVISION=2.3.x-dev/g" /var/backups/BARRACUDA.sh.txt
  else
    sed -i "s/^_BRANCH_PROVISION=.*/_BRANCH_PROVISION=2.3.x-dev/g" /var/backups/BARRACUDA.sh.txt
  fi
  if [ -e "/var/backups/BARRACUDA.sh.txt" ] ; then
    bash /var/backups/BARRACUDA.sh.txt
  fi
else
  echo "BARRACUDA.sh.txt installer not available - try again"
  exit 1
fi
}

init_start ()
{
  if [ -e "/var/run/boa_run.pid" ] ; then
    echo Another BOA installer is running probably - /var/run/boa_run.pid exists
    exit 1
  elif [ -e "/var/run/boa_wait.pid" ] ; then
    echo Some important system task is running probably - /var/run/boa_wait.pid exists
    exit 1
  else
    touch /var/run/boa_run.pid
    touch /var/run/boa_wait.pid
    mkdir -p /var/backups
    cd /var/backups
    rm -f /var/backups/BARRACUDA.sh*
    rm -f /var/backups/OCTOPUS.sh*
  fi
}

set_pin_priority ()
{
  if [ ! -e "/etc/apt/preferences" ] && [ -e "/etc/issue.net" ] ; then
    _THIS_OS=$(grep "Debian" /etc/issue.net)
    if [[ "$_THIS_OS" =~ "Debian" ]] ; then
      _THIS_OS=Debian
    else
      _THIS_OS=Ubuntu
    fi
    if [ "$_THIS_OS" = "Debian" ] ; then
      _THIS_RV=$(grep "6.0" /etc/issue.net)
      if [[ "$_THIS_RV" =~ "6.0" ]] ; then
        curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/master/aegir/conf/etc-apt-preferences-squeeze.txt" -o /etc/apt/preferences
      else
        curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/master/aegir/conf/etc-apt-preferences-wheezy.txt" -o /etc/apt/preferences
      fi
    elif [ "$_THIS_OS" = "Ubuntu" ] ; then
      curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/master/aegir/conf/etc-apt-preferences-ubuntu.txt" -o /etc/apt/preferences
    fi
    rm -f /var/backups/etc-apt-preferences-*
  fi
  _THIS_DO=$(grep "Ubuntu 12.04" /etc/issue.net)
  if [[ "$_THIS_DO" =~ "Ubuntu 12.04" ]] ; then
    _THIS_DO=YesPrecise
  else
    _THIS_DO=Other
  fi
}

init_finish ()
{
  rm -f /var/run/boa_run.pid
  rm -f /var/run/boa_wait.pid
  rm -f /var/run/manage_ltd_users.pid
  rm -f /var/run/manage_rvm_users.pid
  rm -f /var/backups/BARRACUDA.sh*
  rm -f /var/backups/OCTOPUS.sh*
  rm -f /root/BOA.sh*
  echo
  echo BOA $cmmand completed
  echo Bye
  echo
  if [ "$_NEEDS_UPDATE" = "YES" ] ; then
    rm -f /var/backups/BOA.sh.txt-*
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/BOA.sh.txt" -o /var/backups/BOA.sh.txt-$_NOW
    exec bash /var/backups/BOA.sh.txt-$_NOW &> /dev/null
    rm -f /var/backups/BOA.sh.txt-$_NOW
  fi
  exit 0
}

init_setup ()
{
  if [ "$kind" = "local" ] ; then
    mode="$email"
    email="$fqdn"
  fi
  if [ "$kind" = "public" ] ; then
    if [ "$user" = "ask" ] || [ "$user" = "mini" ] || [ "$user" = "max" ] ; then
      rkey="$mode"
      mode="$user"
      user="o1"
    fi
    check_id $user
  fi
  init_start
  set_pin_priority
  if [ "$cmmand" = "in-head" ] ; then
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "https://raw.githubusercontent.com/omega8cc/boa/master/BARRACUDA.sh.txt" -o BARRACUDA.sh.txt
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "https://raw.githubusercontent.com/omega8cc/boa/master/OCTOPUS.sh.txt" -o OCTOPUS.sh.txt
  elif [ "$cmmand" = "in-stable" ] ; then
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/stable/txt/BARRACUDA.sh.txt" -o BARRACUDA.sh.txt
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/stable/txt/OCTOPUS.sh.txt" -o OCTOPUS.sh.txt
  elif [ "$cmmand" = "in-modern" ] || [ "$cmmand" = "in-vanilla" ] ; then
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "https://raw.githubusercontent.com/omega8cc/boa/2.3.x-dev/BARRACUDA.sh.txt" -o BARRACUDA.sh.txt
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "https://raw.githubusercontent.com/omega8cc/boa/2.3.x-dev/OCTOPUS.sh.txt" -o OCTOPUS.sh.txt
  elif [ "$cmmand" = "in-legacy" ] ; then
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/legacy/txt/BARRACUDA.sh.txt" -o BARRACUDA.sh.txt
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/legacy/txt/OCTOPUS.sh.txt" -o OCTOPUS.sh.txt
  elif [ "$cmmand" = "in-octopus" ] ; then
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/stable/txt/BARRACUDA.sh.txt" -o BARRACUDA.sh.txt
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/stable/txt/OCTOPUS.sh.txt" -o OCTOPUS.sh.txt
  fi
  if [ "$cmmand" = "in-octopus" ] ; then
    _OCTOPUS_ONLY=YES
  else
    barracuda_install
  fi
  octopus_install
  init_finish
}

download_wrapper ()
{
  if [ ! -e "$_THIS_FILE" ] ; then
    echo "I can not connect to files.aegir.cc at the moment"
    echo "I will try again in 15 seconds, please wait..."
    sleep 15
    curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/master/aegir/tools/bin/$(basename "$0")" -o $_THIS_FILE
  fi
}

check_wrapper ()
{
  mkdir -p /var/backups
  rm -f /var/backups/$(basename "$0")-now-*
  _THIS_FILE="/var/backups/$(basename "$0")-now-$_NOW"
  curl -L --max-redirs 10 -k -s --retry 10 --retry-delay 5 -A iCab "http://files.aegir.cc/versions/master/aegir/tools/bin/$(basename "$0")" -o $_THIS_FILE
  if [ ! -e "$_THIS_FILE" ] ; then
    download_wrapper
    if [ ! -e "$_THIS_FILE" ] ; then
      download_wrapper
      if [ ! -e "$_THIS_FILE" ] ; then
        download_wrapper
        if [ ! -e "$_THIS_FILE" ] ; then
          echo "Sorry, I gave up."
          echo "Please check http://stats.pingdom.com/x0s6yrbopal6/801243 and try again later."
          exit 1
        fi
      fi
    fi
  fi
  _BIN_FILE="/usr/local/bin/$(basename "$0")"
  _DATE_TEST=$(grep "### $_TODAY ###" $_THIS_FILE)
  if [[ "$_DATE_TEST" =~ "### $_TODAY ###" ]] ; then
    _NEEDS_UPDATE=NO
  else
    _NEEDS_UPDATE=YES
  fi
}

check_dns_curl ()
{
  if [ ! -e "/etc/resolv.conf" ] ; then
    if [ -e "/var/backups/resolv.conf.vanilla" ] ; then
      cat /var/backups/resolv.conf.vanilla >/etc/resolv.conf
    fi
    echo "nameserver 8.8.8.8" >>/etc/resolv.conf
    echo "nameserver 8.8.4.4" >>/etc/resolv.conf
    check_dns_settings
  else
    check_dns_settings
  fi
  if [ -d "/var/cache/pdnsd" ] ; then
    service pdnsd stop &> /dev/null
    sleep 1
    rm -f /var/cache/pdnsd/pdnsd.cache
    sleep 1
    service pdnsd start &> /dev/null
  fi
  _CURL_TEST=$(curl -L --max-redirs 10 -k -s --retry 3 --retry-delay 10 -I "http://files.aegir.cc" 2> /dev/null)
  if [[ "$_CURL_TEST" =~ "200 OK" ]] ; then
    _DO_NOTHING=YES
  else
    apt-get clean -qq &> /dev/null
    apt-get update -qq &> /dev/null
    apt-get install curl -y --force-yes --reinstall &> /dev/null
  fi
}

check_root ()
{
  if [ `whoami` = "root" ] ; then
    chmod a+w /dev/null
    if [ ! -e "/dev/fd" ] ; then
      if [ -e "/proc/self/fd" ] ; then
        rm -rf /dev/fd
        ln -s /proc/self/fd /dev/fd
      fi
    fi
    sed -i "s/.*173.231.133.190.*//g" /etc/hosts
    sed -i "s/^127.0.0.1.*/127.0.0.1 localhost/g" /etc/hosts
    sed -i "s/.*files.aegir.cc.*//g" /etc/hosts
    echo >>/etc/hosts
    echo "72.251.241.251 files.aegir.cc" >>/etc/hosts
    sed -i "/^$/d" /etc/hosts
  else
    echo "ERROR: This script should be ran as a root user - please `sudo -i` first"
    exit 1
  fi
  _DF_TEST=$(df -kTh / -l | grep '/'| sed 's/\%//g'| awk '{print $6}' 2> /dev/null)
  _DF_TEST=${_DF_TEST//[^0-9]/}
  if [ ! -z "$_DF_TEST" ] && [ "$_DF_TEST" -gt "90" ] ; then
    echo "ERROR: Your disk space is almost full - we can not proceed until it is below 90/100"
    exit 1
  fi
  _L_VM_TEST=`uname -a 2>&1`
  if [[ "$_L_VM_TEST" =~ beng ]] ; then
    if [ -e "/sbin/hdparm" ] ; then
      apt-get remove hdparm -y --force-yes -qq &> /dev/null
      apt-get purge hdparm -y --force-yes -qq &> /dev/null
      apt-get autoremove -y --force-yes -qq &> /dev/null
    fi
    _REMOVE_LINKS="vnstat buagent svscan kerneloops halt hwclock.sh hwclockfirst.sh ifupdown ifupdown-clean klogd mountall-bootclean.sh mountall.sh mountdevsubfs.sh mountkernfs.sh mountnfs-bootclean.sh mountnfs.sh mountoverflowtmp mountvirtfs mtab.sh networking reboot setserial sysstat umountfs umountnfs.sh umountroot urandom"
    for link in $_REMOVE_LINKS
    do
      update-rc.d -f $link remove &> /dev/null
    done
  fi
}

case "$1" in
  in-stable) cmmand="$1"
             kind="$2"
             fqdn="$3"
             email="$4"
             user="$5"
             mode="$6"
             rkey="$7"
             check_root
             check_dns_curl
             check_wrapper
             init_setup
  ;;
  in-head)   cmmand="$1"
             kind="$2"
             fqdn="$3"
             email="$4"
             user="$5"
             mode="$6"
             rkey="$7"
             check_root
             check_dns_curl
             check_wrapper
             init_setup
  ;;
  in-modern) cmmand="$1"
             kind="$2"
             fqdn="$3"
             email="$4"
             user="$5"
             mode="$6"
             rkey="$7"
             check_root
             check_dns_curl
             check_wrapper
             init_setup
  ;;
  in-legacy) cmmand="$1"
             kind="$2"
             fqdn="$3"
             email="$4"
             user="$5"
             mode="$6"
             rkey="$7"
             check_root
             check_dns_curl
             check_wrapper
             init_setup
  ;;
  in-vanilla) cmmand="$1"
             kind="$2"
             fqdn="$3"
             email="$4"
             user="$5"
             mode="$6"
             rkey="$7"
             check_root
             check_dns_curl
             check_wrapper
             init_setup
  ;;
  in-octopus) cmmand="$1"
             email="$2"
             user="$3"
             mode="$4"
             copt="$5"
             csub="$6"
             ccor="$7"
             check_wrapper
             init_setup
  ;;
  *)         echo
             echo "Usage: $(basename "$0") {in-stable|in-head|in-modern|in-legacy|in-vanilla|in-octopus} {local|public} {fqdn} {email} {o1} {ask|mini|max|none} {newrelickey}"
             echo
             exit 1
  ;;
esac
